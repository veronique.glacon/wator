from random import *

def creer_mer_vide(nb_lignes,nb_colonnes):
    '''retourne une mer torique sans poisson à deux dimensions.
     :param:(int)  nb_lignes : nombre de lignes de la grille représentant la mer
     :param:(int)  nb_colonnes : nombre de colonnes de la grille représentant la mer   
    CU: aucune
    >>> creer_mer_vide(5, 3)
    [[[], [], []], [[], [], []], [[], [], []], [[], [], []], [[], [], []]]'''
    mer=[]
    for i in range(nb_lignes):
        list=[]
        for j in range(nb_colonnes):
            list.append([])
        mer.append(list)
    return mer      

def nombre_lignes_mer(mer):
    '''retourne le nombre de lignes de la mer 
     :param:(list)  mer
     CU: aucune
    >>> nombre_lignes_mer(creer_mer_vide(3, 2))
    3'''
    return len(mer)
    
def nombre_colonnes_mer(mer):
    '''retourne le nombre de colonnes de la mer 
     :param:(list)  mer
     CU: aucune
    >>> nombre_colonnes_mer(creer_mer_vide(3, 2))
    2'''
    return len(mer[1])

def creer_mer_aleatoire(nb_lignes,nb_colonnes,duree_gestation_thon,duree_gestation_requin,energie_requin,t,r):
    '''retourne une mer torique sans poisson à deux dimensions.
     :param:(int)  nb_lignes 
     :param:(int)  nb_colonnes 
     :param:(int) duree_gestation_thon
     :param:(int) duree_gestation_requin
     :param:(int) energie_requin
     :param:(float) t proba qu'une case soit un thon
     :param:(float) r proba qu'une case soit un requin
     CU: aucune
    >>> creer_mer_aleatoire(2, 2, 2, 4, 3, 1, 0)
    [[[2], [2]], [[2], [2]]]'''
    l=creer_mer_vide(nb_lignes,nb_colonnes)
    for i in range(nb_lignes):
        for j in range(nb_colonnes):
            x=random()
            if x<t:
                l[i][j]=[duree_gestation_thon]
            elif x>=1-r:
                l[i][j]=[duree_gestation_requin,energie_requin]
    return l

def afficher_mer(mer):
    '''
    Visusaliser la grille avec les cases vides représentées avec _ et
    O pour les cases contenant des cellules
    :param:(liste) grille
    CU: aucune
    effet de bord , affiche la grille
    >>> afficher_mer([[[], [2], []], [[], [4, 3], [2]], [[], [], [4, 3]]])
    _ T _
    _ R T
    _ _ R

    '''
    for i in range(nombre_lignes_mer(mer)):
        for j in range(nombre_colonnes_mer(mer)):
            if len(mer[i][j])==0:
                print('_',end=' ')
            elif len(mer[i][j])==1:
                print('T',end=' ')
            elif len(mer[i][j])==2:
                print('R',end=' ')
        print(' ')

def liste_cases_voisines(mer,l,c):
    '''retourne la listes des cases voisines de la case à la ligne l et la colonne c
     dans l ordre suivant N,S,E et O
     :param:(list) mer
     :param:(int) l, numero de la ligne de la case
     :param:(int) c, numero de la colonne de la case
     >>> liste_cases_voisines([[[],[2],[]],[[],[4,3],[2]],[[],[],[4,3]]],1,2) 
     [[], [4, 3], [4, 3], []]
     '''   
    if (l==(nombre_lignes_mer(mer) -1)) and (c==(nombre_colonnes_mer(mer) -1)):
        l= [mer[l-1][c],mer[0][c],mer[l][0],mer[l][c-1]]
    elif l==nombre_lignes_mer(mer)-1:
        l= [mer[l-1][c],mer[0][c],mer[l][c+1],mer[l][c-1]]
    elif c==nombre_colonnes_mer(mer)-1:
        l=[mer[l-1][c],mer[l+1][c],mer[l][c-1],mer[l][0]]
    else:
        l=[mer[l-1][c],mer[l+1][c],mer[l][c+1],mer[l][c-1]]
    return l

def deplacement_aleatoire_case(voisins,critere):
    '''donne aleatoire une case voisines correspondant au critère demandé
     :param:(list) voisins
     :param:(chr) critère (vide,thon,requin)
     :return: (chr) deplacement aléatoitoire choisi :nord, sud, est ou ouest
     >>> deplacement_aleatoire_case([[2],[4,3],[4,3],[]],'thon')
     'nord'
     '''  
    if critere=='vide':
        longueur_liste=0
    elif critere=='thon':
        longueur_liste=1
    elif critere=='requin':
        longueur_liste=2
    liste_possible=[]
    choix=9
    for i in range(4):
        if len(voisins[i])==longueur_liste:
           liste_possible.append(i)
    if len(liste_possible)==0:
        return 'aucun'
    else:
        choix=liste_possible[randint(0,len(liste_possible)-1)]
    if choix==0:
        return 'nord'
    elif choix==1:
        return 'sud'
    elif choix==2:
        return 'est'
    elif choix==3:
        return 'ouest'

def deplacer_case(mer,l,c,lieu):
    '''deplace la case à la ligne l et colonne n de la mer au lieu demandé
     :param:(list) mer
     :param:(int) l, numero de la ligne de la case
     :param:(int) c, numero de la colonne de la case
     :param:(chr) lieu nord, sud, est, ouest ou aucun
     effet de bord :modifie l état de la mer
     '''
    if lieu=='nord':
        mer[l-1][c]=mer[l][c]
    elif lieu=='sud':
        if l==nombre_lignes_mer(mer)-1:
            mer[0][c]=mer[l][c]
        else:
            mer[l+1][c]=mer[l][c]
    elif lieu=='ouest':        
        mer[l][c-1]=mer[l][c]
    elif lieu=='est':
        if c==nombre_colonnes_mer(mer)-1:
            mer[l][0]=mer[l][c]
        else:
            mer[l][c+1]=mer[l][c]
    if lieu!='aucun':
        mer[l][c]=[]
       
    
def comportement_thon(mer,l,c):
    '''retourne le comportement d'un thon situé a la ligne l et colonnes c
     :param:(list) mer
     :param:(int) l, numero de la ligne de la case
     :param:(int) c, numero de la colonne de la case
     effet de bord :modifie l'état de la mer'''      
    cases_voisines=liste_cases_voisines(mer,l,c)
    deplacement=deplacement_aleatoire_case(cases_voisines,'vide')
    nouveau_temps_gestation=mer[l][c][0]-1
    if nouveau_temps_gestation==0:
        mer[l][c]=[duree_gestation_thon]
    else:
        mer[l][c]=[nouveau_temps_gestation]
    deplacer_case(mer,l,c,deplacement)
    if nouveau_temps_gestation==0:
        mer[l][c]=[duree_gestation_thon]
    
        
def comportement_requin(mer,l,c):
    '''retourne le comportement d'un requin situé a la ligne l et colonnes c
     :param:(list) mer
     :param:(int) l, numero de la ligne de la case
     :param:(int) c, numero de la colonne de la case
     effet de bord :modifie l'état de la mer'''        
    cases_voisines=liste_cases_voisines(mer,l,c)
    deplacement_vers_thon=deplacement_aleatoire_case(cases_voisines,'thon')
    nouvelle_energie=mer[l][c][1]-1
    nouveau_temps_gestation=mer[l][c][0]-1
    if deplacement_vers_thon !='aucun':
        nouvelle_energie=energie_requin
        deplacement=deplacement_vers_thon
    else:
        deplacement=deplacement_aleatoire_case(cases_voisines,'vide')
    if nouvelle_energie==0:
        mer[l][c]=[]
    else:
        if nouveau_temps_gestation==0:
            mer[l][c]=[duree_gestation_requin,nouvelle_energie]
        else:
            mer[l][c]=[nouveau_temps_gestation,nouvelle_energie]
        deplacer_case(mer,l,c,deplacement)
        if nouveau_temps_gestation==0:
            mer[l][c]=[duree_gestation_requin,energie_requin]


def simulation_pas_a_pas(nombre_pas,nb_lignes_mer,nb_colonnes_mer,duree_gestation_thon,duree_gestation_requin,energie_requin):
    '''simule aléatoirement une mer retourne la nouvelle mer après avoir effectué nombre de pas étape de simulation
     :param:(int) nombre_pas nombre d étapes
     :param:(int) nb_lignes nombre de lignes de la mer
     :param:(int) nb_colonnes nombre de colonnes de la mer
     :param:(int) duree_gestation_thon
     :param:(int) duree_gestation_requin
     :param:(int) energie_requin
     effet de bord : modifie la mer et affiche la mer à chaque étape'''       
    mer=creer_mer_aleatoire(nb_lignes_mer,nb_colonnes_mer,duree_gestation_thon,duree_gestation_requin,energie_requin,0.3,0.1)
    afficher_mer(mer)
    for i in range(nombre_pas):
        l=randint(0,nb_lignes_mer-1)
        c=randint(0,nb_colonnes_mer-1)
        #print(l,c)
        if len(mer[l][c])==1:
            comportement_thon(mer,l,c)
        elif len(mer[l][c])==2:
            comportement_requin(mer,l,c)
        afficher_mer(mer)    

import time

def compter_poisson(mer,critere):
    '''donne le nombre de poissons correspondant ayant le critère demandé dans la mer
     :param:(list) mer
     :param:(chr) critère (thon,requin)
     :return:(int)
     '''
    nb=0
    for l in range(nombre_lignes_mer(mer)):
        for c in range(nombre_colonnes_mer(mer)):
            if critere=='thon':
                if len(mer[l][c])==1:
                    nb=nb+1
            if critere=='requin':
                if len(mer[l][c])==2:
                    nb=nb+1
    return nb

def simulation_par_centaines_pas(nombre_centaine_pas,nb_lignes_mer,nb_colonnes_mer,duree_gestation_thon,duree_gestation_requin,energie_requin):
    '''simule aléatoirement une mer retourne la nouvelle mer après avoir effectué nombre de pas étape de simulation
     :param:(int) nombre_centaine_pas nombre de centaines d'étapes
     :param:(int) nb_lignes nombre de lignes de la mer
     :param:(int) nb_colonnes nombre de colonnes de la mer
     :param:(int) duree_gestation_thon
     :param:(int) duree_gestation_requin
     :param:(int) energie_requin
     return list liste de tuple pas, nombre de thons, nombre de requins
     effet de bord : modifie la mer et affiche la mer à chaque étape'''
    mer=creer_mer_aleatoire(nb_lignes_mer,nb_colonnes_mer,duree_gestation_thon,duree_gestation_requin,energie_requin,0.3,0.1)
    liste=[(0,compter_poisson(mer,'thon'),compter_poisson(mer,'requin'))]
    #print(liste)
    afficher_mer(mer)
    for j in range(nombre_centaine_pas):
        for i in range(100):
            l=randint(0,nb_lignes_mer-1)
            c=randint(0,nb_colonnes_mer-1)
            #print(l,c)
            if len(mer[l][c])==1:
                comportement_thon(mer,l,c)
            elif len(mer[l][c])==2:
                comportement_requin(mer,l,c)
            liste.append((j*100+i+1,compter_poisson(mer,'thon'),compter_poisson(mer,'requin')))
        #print(liste)
        afficher_mer(mer)
        time.sleep(0.1)
    return liste

import pylab


energie_requin=3
duree_gestation_thon=2
duree_gestation_requin=5
liste_temps=simulation_par_centaines_pas(1000,25,25,2,5,3)
data_x=[i[0] for i in liste_temps]
data_y=[i[1] for i in liste_temps]
data_z=[i[2] for i in liste_temps]
pylab.plot(data_x,data_y)
pylab.plot(data_x,data_z)
pylab.title('Nombre de poissons par espace au cours de la simulation')
pylab.xlabel('nombre de pas')
pylab.ylabel('nombre de poissons')
pylab.show()

import doctest
doctest.testmod(verbose=True)

     